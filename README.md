**Einführung**

`batchrouter` ist ein Skript, der es ermöglicht, aus einer großen Anzahl von Quelle-Ziel-Relationen, die in einer kommaseparierten Datei vorliegen, kürzeste Wege zu erzeugen. Das Skript basiert auf einer lokal ausgeführten **OSRM**-Instanz. Die Dokumentation bezieht sich auf die Anwendung unter einem Linux-System.

Verwendete Pakete für `batchrouter`:
- `pandas`
- `ast`
- `json`
- `geojson`
- `shapely`
- `csv`
- `requests`
- `haversine`

**OSRM initialisieren**

Für das Routing wird das **OSRM-Backend** benötigt, mit der lokal ein Build vom Git-Clone erstellt wird. Im nachfolgenden Link wird der Buildprozess beschrieben.

https://github.com/Project-OSRM/osrm-backend/wiki/Building-OSRM

Mit `test_pairs.csv` wird eine Beispieldatei bereitgestellt, die über 58.960 Quelle-Ziel-Paare mit der Anzahl an Ortsveränderungen pro QZ-Relation in der Stadt Leipzig darstellen.

Zunächst wird für das betrachtete Gebiet nach dem OSRM-build ein routingfähiges OSM-Netz erzeugt. Dafür werden zunächst OSM-Daten von der [https://download.geofabrik.de/](Geofabrik) bezogen. 

```
wget http://download.geofabrik.de/europe/germany/sachsen-latest.osm.pbf
```

Als nächstes wird die OSM-Datei mit **OSRM** aufbereitet. Hierbei wird das Netzwerk für das Verkehrsmittel vorbereitet, weswegen im nachfolgenden Beispiel das Fahrradprofil `bicycle.lua` gewählt wird. 

Für die Executables `osrm-extract`, `osrm-partition` und `osrm-customize` soll im unten stehenden Codeblock auf die jeweiligen Pfade verwiesen werden, die sich im Beispiel unter `osrm-backend/build/` befinden.

```
osrm-backend/build/osrm-extract osm/sachsen-latest.osm.pbf -p profiles/bicycle.lua
osrm-backend/build/osrm-partition osm/sachsen-latest.osrm
osrm-backend/build/osrm-customize osm/sachsen-latest.osrm
```

Als nächstes wird die **OSRM Backend-Instanz** gestartet. Standardmäßig ist das Backend unter `0.0.0.0:5000` oder `localhost:5000` erreichbar.

```
osrm-backend/build/osrm-routed --algorithm=MLD /osm/sachsen-latest.osrm
```

**batchrouter**

`batchrouter.py` wurde als Kommandozeilentool entworfen, der Start- und Endkoordinatenpaare routet und die Routen im `WKT`-Format ausgibt. Unterstützt werden Koordinaten nach dem WGS 84-Koordinatensystem `EPSG:4326`, die in diesem Format in der csv vorliegen müssen.

Das Skript unterstützt neben der Ausgabe der gerouteten Strecke auch die Ausgabe der Luftlinienentfernung.

Damit geroutet werden kann, müssen die csv-Spalten wie folgt vorliegen:

| ... | start_lon | start_lat | end_lon | end_lat | ... |
| ------ | ------ | ------ | ------ | ------ | ------ |
| ... | 12.3722527965003 | 51.311887743686 | 12.2689048485707 | 51.2532483856415 | ... |
| ... | 12.3722527965003 | 51.311887743686 | 12.2724842705263 | 51.253178573715 | ... |

Ausgeführt wird **batchrouter** mit folgenden Parametern:

`-p --path` Pfad und Dateiname der auszugebenden csv-Datei

`-r --routing` Pfad der csv-Datei, die die zu routenden Trips enthält

```
python batchrouter.py -p home/iprk/Desktop/output.csv -r test_pairs.csv

```

Während des Routings wird der Fortschritt in Prozent angezeigt.

**Ideen für eine Erweiterung**

- Definition eigener Spaltenbezeichnungen für die Start- und Endkoordinaten
- Definition abweichender Routingserver
- Wahl des zu routenden Verkehrsmittels oder Verkehrsmittelprofile
- Einbindung anderer Routing-Engines
- Verarbeitung der Routen im WKT-Format zur Erzeugung einer Verkehrsmengenkarte: Schon bei geringen QZ-Relationen führt eine Aufsplittung eines `LineString` zu einer sehr hohen Zahl von einzelnen `LineString`-Bestandteilen, was bei QGIS zu einem hohen Speicher- und Rechenbedarf führt. Ziel wäre es hier, die `LineString`-Bestandteile bereits mit einem Python-Skript automatisiert zu aggregieren.
