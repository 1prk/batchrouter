import pandas as pd
import requests
from haversine import haversine, Unit
import ast
import json
import geojson
from shapely.geometry import shape
import csv
import argparse


parser = argparse.ArgumentParser(description='Batchrouter for OSRM')
parser.add_argument('-p', '--path', type= str, required=True, help= 'specify path for output file')
parser.add_argument('-r', '--routing', type=str, required=True, help='specify path for routing file')
parser.parse_args()

#OSRM client needs to be run before executing this code
def osrm(start_pt, end_pt):
    coordinate_list = [str(start_pt)[1:-1].replace(' ', ''), str(end_pt)[1:-1].replace(' ', '')]
    coordinate_str = ';'.join(coordinate_list)
    service = 'http://0.0.0.0:5000/'
    service_url = 'route/v1/driving/{}?overview=full'.format(coordinate_str)
    request_url = service + service_url
    payload = {"geometries": "geojson"}
    r = requests.get(request_url, params=payload)
    results = r.json()
    for trips in results['routes']:
        trip_data = trips['geometry']
        t_data = str(trip_data)
        s = json.dumps(ast.literal_eval(t_data))
        g1 = geojson.loads(s)
        g2 = shape(g1)
        return g2

def routing(csv_input, csv_output):
    donkey = pd.read_csv(csv_input, low_memory=False)
    with open(csv_output, 'a') as f:
        w = csv.writer(f)
        header = donkey.columns.values.tolist()
        header2 = ["haversine", "wkt"]
        header.extend(header2)
        w.writerow(header)
        for i in range(len(donkey)):
            start = donkey.loc[i, 'start_lon'], donkey.loc[i, 'start_lat']
            end = donkey.loc[i, 'end_lon'], donkey.loc[i, 'end_lat']
            hav = haversine(start[::-1], end[::-1], unit='m')
            df = donkey.loc[i]
            csv_l = df.values.tolist()
            exp = [hav, str(osrm(start, end))]
            csv_l.extend(exp)
            w.writerow(csv_l)


args = parser.parse_args()
donkey = pd.read_csv(args.routing, low_memory=False)
with open(args.path, 'a') as f:
    w = csv.writer(f)
    header = donkey.columns.values.tolist()
    header2 = ["haversine", "wkt"]
    header.extend(header2)
    w.writerow(header)
    for i in range(len(donkey)):
        start = donkey.loc[i, 'start_lon'], donkey.loc[i, 'start_lat']
        end = donkey.loc[i, 'end_lon'], donkey.loc[i, 'end_lat']
        hav = haversine(start[::-1], end[::-1], unit='m')
        df = donkey.loc[i]
        csv_l = df.values.tolist()
        exp = [hav, str(osrm(start,end))]
        csv_l.extend(exp)
        #print('routed OD relations: %s' % i)
        #print('remaining OD relations: %s' % str(len(donkey) - i))
        print('Routing progress: %s %%' % str(round(i*100/len(donkey),3)), end="\r")
        w.writerow(csv_l)
