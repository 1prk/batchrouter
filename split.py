import pandas as pd
import requests
from itertools import zip_longest
from haversine import haversine, Unit
import ast
import json
import geojson
from shapely.geometry import shape, LineString
import matplotlib.pyplot as plt


def input(path):
    inpt = pd.read_csv(path, low_memory=False)
    return inpt

def coords(input):
    for i in range(len(input)):
        start = input.loc[i, 'start_lon'], input.loc[i, 'start_lat']
        end = input.loc[i, 'end_lon'], input.loc[i, 'end_lat']
        se = [str(start)[1:-1].replace(' ', ''), str(end)[1:-1].replace(' ', '')]
        coordinate_str = ';'.join(se)
        return coordinate_str

def routing(coords):
    service = 'http://0.0.0.0:5000/'
    service_url = 'route/v1/driving/{}?overview=full'.format(coords)
    request_url = service + service_url
    payload = {"geometries": "geojson"}
    r = requests.get(request_url, params=payload)
    results = r.json()
    for trips in results['routes']:
        trip_data = trips['geometry']
        t_data = str(trip_data)
        s = json.dumps(ast.literal_eval(t_data))
        g1 = geojson.loads(s)
        g2 = shape(g1)
        return g2

def linesplit(linestring):
    out = list(shape(linestring).coords)
    return out

def split(wktlist):
    for  i in range(len(wkt))

wkt = linesplit(routing(coords(input('test_pairs.csv'))))


print(split)